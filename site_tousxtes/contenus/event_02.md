Invitée par le 75, la Maison Amazone propose une rencontre avec la BIB St Josse et son fond « BTGE: Bibliothèque en tous genres » et l'ASBL Touche pas à Ma Pote: TPAMP.

Un tour d’horizon des outils de lutte contre les discriminations de genre et le harcèlement sexiste, des projets menés par les associations et une session de lecture d’extraits d’ouvrages du fonds BTGE vous seront proposés, suivi d’une session Q/R avec la possibilité de consulter des ouvrages du fonds BTGE à la fin de la conférence.

<u>Amazone</u> est le carrefour de l'égalité de genre, situé au cœur de Bruxelles.
Amazone est à la fois une asbl et une Maison d’associations qui a été
fondée en 1995: un concept visionnaire qui a ensuite été adopté dans
plusieurs pays. Créer une synergie et soutenir le mouvement des femmes
sont des mots-clés pour Amazone. Ensemble, nous sommes fort·e·s !  En
tant que cœur battant du mouvement des femmes, la Maison Amazone jette
des ponts entre la société civile, la politique et le monde
universitaire. [+](https://linktr.ee/Amazone_be)

<u>Touche pas à Ma Pote: TPAMP</u> fut tout d’abord une campagne contre le harcèlement de rue et le sexisme au quotidien. Lancée par le magazine ELLE Belgique en 2012, elle est devenue par la suite une entité indépendante sous la forme d’une ASBL bruxelloise. On ne peut pas laisser les femmes et les minorités de genre seules face à cette forme de violence que représentent le harcèlement de rue et les attitudes sexistes en général. C’est pourquoi TPAMP prône la mise en place de 
campagnes de sensibilisation (ou le fait elle-même !), d’éducation et de formations, des sanctions et une loi contre le sexisme.  [+](https://tpamp.be)

<u>BTGE: Bibliothèque en tous genres</u> est une collection, Inauguré en 2009, riche de plus d’un millier d’ouvrages, abordant principalement les questions liées au genre mais également à la lutte contre toute forme de discrimination. Bib Josse vous propose ce fonds, représenté tant en section adultes qu’en section jeunesse, au travers de documentaires et de fictions identifiables par une pastille blanche apposée au dos de l’ouvrage.

Dans un souci de sensibilisation et de promotion d’idées égalitaires, la politique d’acquisition de la bibliothèque écarte autant que possible les ouvrages véhiculant des stéréotypes et privilégie tout-e autrice, auteur ou maison d’édition qui s’engage dans leur déconstruction.

L’objectif est de lutter contre les discriminations en promouvant le droit à la différence. La bibliothèque fait connaître ce fonds lors d’expositions, colloques, débats et heures du conte, en collaboration avec d’autres associations et institutions impliquées dans ces thématiques.  [+](https://bibliothequedesaintjosse.com/2020/04/06/une-bibliotheque-en-tous-genres-des-livres-et-des-projets-pour-lutter-contre-les-inegalites-et-les-discriminations/)
