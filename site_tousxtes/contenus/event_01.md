Sur l'invitation de Mélanie Godin, enseignante au 75, dans le cadre de son cours de littérature contemporaine, Camille Circlude est venu.e présenter le travail de la collective de typographes <u>Bye Bye Binary</u>.

<u>Caroline Dath°Camille Circlude</u> est graphiste au sein du studio Kidnap Your Designer, qu’iel anime avec Damien Safie et Esther Poch.

Caroline°Camille est enseignanl à l’erg, depuis 2009, au sein de l’atelier de Graphisme.
Iel a créé le module « Digital Non Binaire » avec Stéphanie Vilayphiou (OSP) et le module *«Du concept éditorial à l’impression»*.

Camille°Caroline met en œuvre une démarche réflexive sur des pratiques pédagogiques qu’iel expérimente en initiant des workshops (Summer School Art Passing Trans*, Trans//Border, Bye Bye Binary,…), en animant avec d’autres ou 
encore en écrivant*.

Depuis 2017, avec Loraine Furter et Xavier Gorgol, iel fait partie du groupe de recherche Teaching To Transgress* qui est une expérimentation sur les questions de genres, de post-colonialisme, de féminismes intersectionnels, queer et situés dans la pédagogie et la pratique des arts. En 2019-2022, ce projet a pris la forme d’un programme européen Erasmus Plus Teaching To Transgress Toolbox.

Depuis 2018, Camille°Caroline est membre de la collective Bye Bye Binary 
active sur les questions de typographie inclusive et non-binaire.



Camille°Caroline poursuit actuellement, 2020-2022, un Master de spécialisation en études de genre inter-universitaire à Bruxelles pour lequel iel mène un mémoire recherche sur la *«Révolution typographique inclusive et le non-binarisme politique»*.
