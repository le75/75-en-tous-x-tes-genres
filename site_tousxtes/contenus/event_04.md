<u>Just For The Record</u> est un projet questionnant la représentation des genres dans les nouveaux médias et les outils d’écriture/de partage du savoir tels que Wikipédia, et l’influence de cette représentation sur l’écriture de l’histoire et du savoir.

Pour approcher ces question et encourager plus de diversité en ligne, Just For The Record propose une série de rencontres thématiques à partir de janvier 2016, avec des invité·e·s, des présentations et des sessions d’édition collectives sur Wikipedia. Sur base des ces expériences, nous explorons actuellement de nouveaux formats interrogeant les façons dont l’histoire est écrite, racontée, chantés, dessinée, criée…

Nous recevrons le 19 Avril Sarah Magnan et Loraine Furter.
