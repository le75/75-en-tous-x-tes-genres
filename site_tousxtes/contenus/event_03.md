<u>Isola</u> est une série de podcast qui vous emmène dans l’invisible; ce qu'on ne voit pas mais qui nous relie, réalisée par Laura Krsmanovic. Les images de l'épisode SOMOS on été prise par Lydie Nesvadba. Avec Alexia, Aurélio, Elsa, Emmanuelle, Fio, Harry, Louka, Mya, Nell, Reb, Rose, Sacha G. et Sacha P. Somos est réalisé en collaboration avec l'ASBL Transkids

« Je crois fortement qu’on est ni homme, ni femme, juste un corps vivant. » Paul B. Preciado Que veut dire être soi-même ? Qui peut répondre à la question « qui suis-je » sans trembler, sans hésiter? Lorsqu’il s’agit de se « définir » comme on dit, il est tentant d’avoir recours à des identités figées, reçues, auxquelles il nous est demandé, implicitement ou explicitement, de nous conformer. Nous mettons en avant nos traits de caractère, nos façons d’être, nos habitudes, ce qui demeure fixe et nous identifie ainsi auprès des autres. Mais la vraie conformité n’est-elle pas celle que 
nous nous devons à nous-mêmes ? Etre soi-même, en réalité, ce n’est pas 
une question d’identité, mais de vérité personnelle. Être soi-même, ce 
n’est pas une donnée, c’est une aventure, une expérience, faite de doute
 et d’accomplissement, une expérience profondément humaine. L’humanité 
est un corps vivant qui inlassablement cherche sa vérité.

<u>SOMOS</u> donne la parole à 13 jeunes, âgé•es de 9 à 27 ans, qui déconstruisent les normes en affirmant leur identité. Iels sont trans, non-binaires, ou créatif•ve•s sur le plan du genre et prennent la parole pour exposer leurs vécus et ressentis. Leurs mots vifs et honnêtes percutent. SOMOS nous invite ainsi à écouter des voix trop souvent tues, à prendre conscience de la beauté de devenir soi.
