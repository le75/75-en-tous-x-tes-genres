<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"/>
    <title>∴⁖∴75 en tous·x·tes genres∴⁖∴</title>

		<?php
				$style = 'main.css';
		?>
    <link rel="stylesheet" media="all" href="css/<?= $style ?>" type="text/css">
  </head>
  <body>
    <?php
         include "libs/Parsedown.php";
         $Parsedown = new Parsedown();
     ?>
<div class="content">
  <div class="intro">
    <div class="logo-entier">
          <div class="logo">
            75 en tous·x·tes genres</div>
          <div class="sous-titre">Penser l’école inclusive⁖∴⁖</div>
        </div>
          <div class="introduction">
          <?php
            $text = file_get_contents('contenus/intro.md');
            echo $Parsedown->text($text);
          ?>
        </div>
  </div>

    <details>
      <summary>
        <span class="infos">Camille Circlude </span>
        <span class="date">16·12·21⁖∴ 18h-20h </span>
        <span class="lieu">Debecker, local 1 & 2</span>
      </summary>

      <div class="infosbox"><?php
        $text = file_get_contents('contenus/event_01.md');
        echo $Parsedown->text($text);
      ?></div>
      </details>


    <details>
      <summary>
        <span class="infos"> Amazone </span>
        <span class="date">17·02·22⁖∴ 18h-20h </span>
        <span class="lieu">Debecker, local 1 & 2</span>
      </summary>

      <div class="infosbox"><?php
        $text = file_get_contents('contenus/event_02.md');
        echo $Parsedown->text($text);
      ?></div>
    </details>


    <details>
      <summary>
        <span class="infos"> Somos</span>
        <span class="date">18·03·22 - 19·03·22⁖∴ </span>
        <span class="lieu">Debecker, local 2</span>
      </summary>
      <div class="infosbox"><?php
        $text = file_get_contents('contenus/event_03.md');
        echo $Parsedown->text($text);
      ?></div>
    </details>


    <details>
      <summary>
        <span class="infos"> Just for the record</span>
        <span class="date">26·04·22⁖∴ </span>
        <span class="lieu">Debecker</span>
      </summary>
      <div class="infosbox"><?php
        $text = file_get_contents('contenus/event_04.md');
        echo $Parsedown->text($text);
      ?></div>
    </details>

      <details>
        <summary>
          <span class="infos"> Bledarte</span>
          <span class="date">19·05·22⁖∴ </span>
          <span class="lieu">Debecker</span>
        </summary>
        <div class="infosbox"><?php
          $text = file_get_contents('contenus/event_05.md');
          echo $Parsedown->text($text);
        ?></div>
        </details>

          <details>
            <summary> <a href="#">∴⁖∴</a></summary>
            <!-- <div class="infosbox"><?php
              $text = file_get_contents('contenus/colophon.md');
              echo $Parsedown->text($text);
            ?></div> -->
            </details>

</div>





  </body>
</html>
